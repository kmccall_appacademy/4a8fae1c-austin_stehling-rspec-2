def reverser(&prc)
  prc.call.split.map{|x| x.reverse}.join(" ")
end

def adder(value = 1, &prc)
  value + prc.call
end

def repeater(repeat = 1, &prc)
  repeat.times do
    prc.call
  end
end
